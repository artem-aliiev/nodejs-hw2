const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
    const userId = req.user._id;
    const {limit='5', offset='0'} = req.query;
    const notes = await Note.find(
        {userId: userId},
        {__v: 0},
        {
            limit: parseInt(limit) > 100 ? 5 : parseInt(limit),
            skip: parseInt(offset),
        },
    );

    res.json({notes});
};

module.exports.createNote = async (req, res) => {
    const userId = req.user._id;
    const {text} = req.body;

    const note = new Note({
        userId: userId,
        text,
    });

    await note.save();

    res.status(200).json({message: 'Success'});
};

module.exports.getNote = async (req, res) => {
    const noteId = req.params.id;
    const note = await Note.findOne({_id: noteId}, {__v: 0});

    res.status(200).json({note});
};

module.exports.updateNote = async (req, res) => {
    const noteId = req.params.id;
    const {text} = req.body;

    await Note.findByIdAndUpdate(noteId, {text});

    res.status(200).json({message: 'Success'});
};

module.exports.changeCompletedValue = async (req, res) => {
    const noteId = req.params.id;
    const note = await Note.findById(noteId);

    note.completed = !note.completed;

    await note.save();

    res.status(200).json({message: 'Success'});
};

module.exports.deleteNote = async (req, res) => {
    const noteId = req.params.id;

    await Note.findByIdAndDelete(noteId);

    res.status(200).json({message: 'Success'});
};
