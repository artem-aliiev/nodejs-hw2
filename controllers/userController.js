const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

module.exports.getUser = async (req, res) => {
    const userId = req.user._id;
    const user = await User.findById(userId);

    res.status(200).json({
        'user': {
            '_id': user._id,
            'username': user.username,
            'createdDate': user.createdDate,
        },
    });
};

module.exports.deleteUser = async (req, res) => {
    const userId = req.user._id;
    await User.deleteOne({'_id': userId});
    await Note.deleteMany({'userId': userId});

    res.status(200).json({message:'Success'});
};

module.exports.changePassword = async (req, res) => {
    const {oldPassword, newPassword} = req.body;
    const userId = req.user._id;
    const user = await User.findById(userId);

    if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
        return res.status(400).json({
            'message': `Wrong password!`,
        });
    }

    await User.findOneAndUpdate(
        {userId},
        {password: await bcrypt.hash(newPassword, 10)},
    );

    res.status(200).json({message:'Success'});
};
