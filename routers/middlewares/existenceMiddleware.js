const {Note} = require('../../models/noteModel');

module.exports.existenceMiddleware = async (req, res, next) => {
    const noteId = req.params.id;
    const userId = req.user._id;
    const note = await Note.findOne({_id: noteId});

    if (userId.toString() !== note.userId.toString()) {
        return res.status(400).json({
            message: 'This record does not belong to this account!',
        });
    }

    next();
};
