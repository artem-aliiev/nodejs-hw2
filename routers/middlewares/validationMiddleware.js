const Joi = require('joi');
const passwordPattern = new RegExp('^[a-zA-Z0-9]{5,30}$');

module.exports.validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .required(),

        password: Joi.string()
            .pattern(passwordPattern)
    });

    await schema.validateAsync(req.body);
    next();
}
