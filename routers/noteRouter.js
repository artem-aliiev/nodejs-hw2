const express = require('express');
const router = express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {existenceMiddleware} = require('./middlewares/existenceMiddleware')
const {
    getNotes,
    createNote,
    getNote,
    updateNote,
    changeCompletedValue,
    deleteNote,
} = require('../controllers/noteController');

router.get('/',
    authMiddleware,
    asyncWrapper(getNotes)
);

router.post('/',
    authMiddleware,
    asyncWrapper(createNote)
);

router.get('/:id',
    authMiddleware,
    asyncWrapper(existenceMiddleware),
    asyncWrapper(getNote),
);

router.put('/:id',
    authMiddleware,
    asyncWrapper(existenceMiddleware),
    asyncWrapper(updateNote),
);

router.patch('/:id',
    authMiddleware,
    asyncWrapper(existenceMiddleware),
    asyncWrapper(changeCompletedValue),
);

router.delete('/:id',
    authMiddleware,
    asyncWrapper(existenceMiddleware),
    asyncWrapper(deleteNote),
);

module.exports = router;
